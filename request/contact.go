package request

type User struct {
	Name             string `json:"name"`
	Email            string `json:"email"`
	TwitterId        string `json:"twitter_id"`
	UniqueExternalId string `json:"unique_external_id"`
	Address          string `json:"address"`
}
