package responses

type User struct {
	Login           string `json:"login"`
	ID              int    `json:"id"`
	Name            string `json:"name"`
	Company         string `json:"company"`
	Location        string `json:"location"`
	Email           string `json:"email"`
	TwitterUsername string `json:"twitter_username"`
}
