package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"quickbasedemo/request"
	"quickbasedemo/responses"
	"strconv"
)

// newAuthenticatedRequest creates a Request with Authentication header matching the expected authentication for
//the site:
// - github: username:token
// - freshdesk: token:x
func newAuthenticatedRequest(method, url, apiDomain string, body io.Reader) (*http.Request, error) {

	var password string
	switch apiDomain {
	case "github":
		password = os.Getenv("GITHUB_TOKEN")
	case "freshdesk":
		password = os.Getenv("FRESHDESK_TOKEN")
	default:
		return nil, fmt.Errorf("bad API domain")
	}

	req, err := http.NewRequest(method, url, body)
	if err != nil {
		return nil, err
	}

	if apiDomain == "github" {
		req.SetBasicAuth("gppopoff", password)
	} else {
		req.SetBasicAuth(password, "x")
	}

	return req, nil

}

// getUserResponse send GET request to github and converts the json data into responses.User
func getUserResponse(userName string) (*responses.User, error) {
	url := fmt.Sprintf("https://api.github.com/users/%s", userName)

	req, err := newAuthenticatedRequest("GET", url, "github", nil)
	if err != nil {
		return nil, err
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	var u responses.User
	err = json.NewDecoder(resp.Body).Decode(&u)

	return &u, err
}

// postNewContact sends POST request to freshdesk with given contactInfo - []byte - which adds the new contact to
//contact list of a user with domainName
func postNewContact(domainName string, contactInfo []byte) error {
	url := fmt.Sprintf("https://%s.freshdesk.com/api/v2/contacts", domainName)
	body := bytes.NewBuffer(contactInfo)

	req, err := newAuthenticatedRequest("POST", url, "freshdesk", body)
	if err != nil {
		return err
	}

	req.Header.Set("Content-type", "application/json")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}

	if resp.StatusCode == 201 {
		fmt.Println("Contact created successfully!")
	} else {
		fmt.Println("Failed to create contact! Status code:", resp.StatusCode)
		defer resp.Body.Close()

		responseBody, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return err
		}

		log.Println(string(responseBody))
	}

	return nil
}

// updateContactList uses responses.User info collected from getUserResponse with given userName
//, convert it to request.User and calls postNewContact with the new User info - contactInfo - as a []byte
//and domainName
func updateContactList(userName, domainName string) error {
	userResponse, err := getUserResponse(userName)
	if err != nil {
		return err
	}

	user := request.User{
		Name:  userResponse.Name,
		Email: userResponse.Email,
		//Email:            "proben_mail@gmail.com",
		TwitterId:        userResponse.TwitterUsername,
		UniqueExternalId: strconv.Itoa(userResponse.ID),
		Address:          userResponse.Location,
	}
	contactInfo, err := json.Marshal(user)
	if err != nil {
		return err
	}

	err = postNewContact(domainName, contactInfo)
	return err
}

// main function simply checks if there are 2 given arguments to the program and calls updateContactList
func main() {
	if len(os.Args) != 3 {
		log.Fatal("Invalid number of arguments")
	} else {
		userName := os.Args[1]
		domainName := os.Args[2]
		//userName := "gppopoff"
		//domainName := "newaccount1618054557892"

		err := updateContactList(userName, domainName)
		if err != nil {
			log.Fatal("Error accu")
		}
	}

}
